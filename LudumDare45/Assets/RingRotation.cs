﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RingRotation : MonoBehaviour
{
    [SerializeField] float animHeight = 0.2f;
    [SerializeField] float animSpeed = 2.0f;

    void Start()
    {
        RotateThis();
    }

    void RotateThis()
    {
        Vector3 rot = new Vector3(0, 360, 0);
        Vector3 down = this.transform.position;
        Vector3 up = new Vector3(this.transform.position.x, this.transform.position.y + animHeight, this.transform.position.z);
        this.transform.DOMove(up, animSpeed).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutQuad);
        this.transform.DORotate(rot, 3f, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
    }
}

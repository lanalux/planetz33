﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Attacks : MonoBehaviour
{
    //[SerializeField] GameObject attachedBuilding;
    [SerializeField] GameOver gameOverScript;
    public delegate void StartExplosion();
    public static event StartExplosion startExplosion;
    [SerializeField] List<GameObject> buildings = new List<GameObject>();
    [SerializeField] BuildCrystal attackCrystal, defenseCrystal;
    [SerializeField] AudioSource explosionSFX;
    [SerializeField] CanvasGroup flashCG;


    float enemyDamage = 0.10f; // ten percent
    float enemyDamage1 = 0.09f;
    float enemyDamage2 = 0.07f;
    float enemyDamage3 = 0.05f;


    private void Start()
    {
        StartCoroutine(RandomAttack());
    }

    public IEnumerator RandomAttack()
    {
        while (true)
        {
            float rand = Random.Range(3f, 6f);
            switch (attackCrystal.thisBuilding.currentBuilding)
            {
                case 1:
                    rand = Random.Range(6f, 10f);
                    break;
                case 2:
                    rand = Random.Range(10f, 12f);
                    break;
                case 3:
                    rand = Random.Range(12f, 16f);
                    break;
            }
            yield return new WaitForSeconds(rand);
            if (Static.interactionOn)
            {
                if (!Static.isDay)
                {
                    if (startExplosion != null)
                    {
                        startExplosion();
                        CauseDamage();
                    }
                }
            }
        }
    }

    void CauseDamage()
    {
        int rand = Random.Range(0, buildings.Count - 1);
        BuildCrystal crystalScript = buildings[rand].transform.GetChild(0).GetComponent<BuildCrystal>();

        explosionSFX.Play(0);
        flashCG.DOFade(1.0f, 0.2f).OnComplete(() =>
        {
            flashCG.DOFade(0.0f, 0.4f);
        });

        if (!Static.isInside)
        {
            gameOverScript.StartGameOver();
        }
        else
        {

            if (crystalScript != null)
            {
                while (crystalScript.thisBuilding.currentHP <= 0)
                {
                    rand = Random.Range(0, buildings.Count - 1);
                    crystalScript = buildings[rand].transform.GetChild(0).GetComponent<BuildCrystal>();
                }


                switch (defenseCrystal.thisBuilding.currentBuilding) {
                    case 1:
                        enemyDamage = enemyDamage1;
                        break;
                    case 2:
                        enemyDamage = enemyDamage2;
                        break;
                    case 3:
                        enemyDamage = enemyDamage3;
                        break;
                }

                crystalScript.thisBuilding.currentHP -= crystalScript.thisBuilding.maxHP * enemyDamage;

                if (crystalScript.thisBuilding.currentHP <= 0)
                {
                    if (crystalScript.buildingName == "Base")
                    {
                        RemoveBase();
                    } else 
                    {
                        foreach (GameObject building in crystalScript.thisBuilding.buildings)
                        {
                            building.SetActive(false);
                        }
                        crystalScript.thisBuilding.buildings[0].SetActive(true);
                        crystalScript.thisBuilding.currentBuilding = 0;
                        crystalScript.thisBuilding.currentHP = 0;
                        crystalScript.thisBuilding.maxHP = 0;
                    }
                }
                Debug.Log(crystalScript.buildingName);
            }
        }
    }

    void RemoveBase()
    {
        Static.isInside = false;
        foreach (GameObject parentBuilding in buildings)
        {
            BuildCrystal crystalScript = parentBuilding.transform.GetChild(0).GetComponent<BuildCrystal>();
            foreach (GameObject building in crystalScript.thisBuilding.buildings)
            {
                building.SetActive(false);
            }
            crystalScript.thisBuilding.buildings[0].SetActive(true);
            crystalScript.thisBuilding.currentBuilding = 0;
            crystalScript.thisBuilding.maxHP = 0;
            crystalScript.thisBuilding.currentHP = 0;
            parentBuilding.SetActive(false);
        }
        buildings[0].SetActive(true);
    }



}

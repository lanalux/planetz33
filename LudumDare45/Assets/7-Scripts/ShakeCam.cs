﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShakeCam : MonoBehaviour
{
    [SerializeField] CanvasGroup flash;

    void Start()
    {
        Attacks.startExplosion += ShakeThis;
    }

    void ShakeThis()
    {
        Vector3 currentPos = this.gameObject.transform.position;
        Vector3 shake1 = new Vector3(0.05f, 0.05f, -0.05f);
        Vector3 shake2 = new Vector3(-0.05f, 0.05f, 0.05f);
        Vector3 shake3 = new Vector3(0.05f, -0.05f, -0.05f);
        Vector3 shake4 = new Vector3(-0.05f, -0.05f, 0.05f);
        Sequence shake = DOTween.Sequence().OnComplete(() =>
        {

        });

        flash.DOFade(1.0f, 0.05f).OnComplete(() =>
        {
            flash.DOFade(0.0f, 0.3f);
        });
        shake.Insert(0, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake1, 0.05f));
        shake.Insert(0.05f, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake2, 0.05f));
        shake.Insert(0.1f, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake3, 0.05f));
        shake.Insert(0.15f, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake4, 0.05f));
        shake.Insert(0.2f, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake3, 0.05f));
        shake.Insert(0.25f, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake2, 0.05f));
        shake.Insert(0.3f, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake1, 0.05f));
        shake.Insert(0.35f, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake4, 0.05f));
        shake.Insert(0.4f, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake1, 0.05f));
        shake.Insert(0.45f, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake3, 0.05f));
        shake.Insert(0.5f, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake2, 0.05f));
        shake.Insert(0.55f, this.gameObject.transform.DOMove(this.gameObject.transform.position + shake4, 0.05f));

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class BuildCrystal : MonoBehaviour
{
    [SerializeField] ControlsMain controlScript;
    [SerializeField] ShowHideBuildOptions showBuildScript;
    public Building thisBuilding;
    public Text constructionType, buildingNameText, maxHPtext;
    public CanvasGroup instructions;
    public List<Text> resourceNeeded = new List<Text>();
    [SerializeField] RectTransform meter;
    public FirstPersonController controller;

    [SerializeField] List<GameObject> tutorialItems = new List<GameObject>();
    [SerializeField] AudioSource hummingSFX;
    

    float animHeight = 0.2f;
    float animSpeed = 2.0f;
    float hpMeterHeight = 10f;
    float hpMeterWidth = 250f;

    bool canConstruct = false;
    bool canRepair = false;

    public string buildingName;

    [SerializeField] AudioSource ding1;
    [SerializeField] AudioSource ding2;
    [SerializeField] AudioSource construction;


    void Start()
    {
        RotateThis();
    }

    void RotateThis()
    {
        Vector3 rot = new Vector3(0, 360, 0);
        Vector3 down = this.transform.position;
        Vector3 up = new Vector3(this.transform.position.x, this.transform.position.y + animHeight, this.transform.position.z);
        this.transform.DOMove(up, animSpeed).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutQuad);
        this.transform.DORotate(rot, 3f, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
    }


    public void HoverBuilding(GameObject currentBuilding)
    {
        buildingNameText.text = buildingName;
        maxHPtext.text = Mathf.Floor(thisBuilding.currentHP).ToString() + "/" + Mathf.Floor(thisBuilding.maxHP).ToString();

        float hpPercent = thisBuilding.currentHP / thisBuilding.maxHP;
        meter.sizeDelta = new Vector2(hpPercent * hpMeterWidth, hpMeterHeight);
        if (hpPercent < 0.1f)
        {
            meter.GetComponent<Image>().color = new Color(1.0f, 0f, 0.4f, 1.0f);
        }
        else
        {
            meter.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        }

        if (thisBuilding.currentBuilding == 0)
        {
            // BUILD
            for (int i = 0; i < resourceNeeded.Count; i++)
            {
                resourceNeeded[i].text = thisBuilding.level1Req[i].ToString();
            }
            if (controlScript.metal >= thisBuilding.level1Req[0] && controlScript.copper >= thisBuilding.level1Req[1] && controlScript.shipParts >= thisBuilding.level1Req[2] && controlScript.alienRocks >= thisBuilding.level1Req[3])
            {
                instructions.alpha = 1.0f;
                canConstruct = true;
                canRepair = false;
            }
            else
            {
                instructions.alpha = 0.5f;
                canConstruct = false;
                canRepair = false;
            }
            constructionType.text = "Build";
        }
        else
        {
            if (thisBuilding.currentHP < thisBuilding.maxHP)
            {
                // REPAIR
                for (int i = 0; i < resourceNeeded.Count; i++)
                {
                    if (thisBuilding.currentBuilding == 1)
                    {
                        resourceNeeded[i].text = thisBuilding.repairs1[i].ToString();
                    }
                    else if (thisBuilding.currentBuilding == 2)
                    {
                        resourceNeeded[i].text = thisBuilding.repairs2[i].ToString();
                    }
                    else
                    {
                        resourceNeeded[i].text = thisBuilding.repairs3[i].ToString();
                    }
                }

                if ((thisBuilding.currentBuilding == 1 && controlScript.metal >= thisBuilding.repairs1[0] && controlScript.copper >= thisBuilding.repairs1[1] && controlScript.shipParts >= thisBuilding.repairs1[2] && controlScript.alienRocks >= thisBuilding.repairs1[3]) || (thisBuilding.currentBuilding == 2 && controlScript.metal >= thisBuilding.repairs2[0] && controlScript.copper >= thisBuilding.repairs2[1] && controlScript.shipParts >= thisBuilding.repairs2[2] && controlScript.alienRocks >= thisBuilding.repairs2[3]) || (thisBuilding.currentBuilding == 3 && controlScript.metal >= thisBuilding.repairs3[0] && controlScript.copper >= thisBuilding.repairs3[1] && controlScript.shipParts >= thisBuilding.repairs3[2] && controlScript.alienRocks >= thisBuilding.repairs3[3]))
                {
                    instructions.alpha = 1.0f;
                    canRepair = true;
                    canConstruct = false;
                }
                else
                {
                    instructions.alpha = 0.5f;
                    canRepair = false;
                    canConstruct = false;
                }
                constructionType.text = "Repair";
            }
            else
            {
                if (thisBuilding.currentBuilding < thisBuilding.buildings.Count - 1)
                {
                    // UPGRADE
                    instructions.alpha = 1.0f;
                    if (thisBuilding.currentBuilding == 1)
                    {
                        for (int i = 0; i < resourceNeeded.Count; i++)
                        {
                            resourceNeeded[i].text = thisBuilding.level2Req[i].ToString();
                        }
                        if (controlScript.metal >= thisBuilding.level2Req[0] && controlScript.copper >= thisBuilding.level2Req[1] && controlScript.shipParts >= thisBuilding.level2Req[2] && controlScript.alienRocks >= thisBuilding.level2Req[3])
                        {
                            instructions.alpha = 1.0f;
                            canConstruct = true;
                            canRepair = false;
                        }
                        else
                        {
                            instructions.alpha = 0.5f;
                            canConstruct = false;
                            canRepair = false;
                        }
                    }
                    else if (thisBuilding.currentBuilding == 2)
                    {
                        for (int i = 0; i < resourceNeeded.Count; i++)
                        {
                            resourceNeeded[i].text = thisBuilding.level3Req[i].ToString();
                        }
                        if (controlScript.metal >= thisBuilding.level3Req[0] && controlScript.copper >= thisBuilding.level3Req[1] && controlScript.shipParts >= thisBuilding.level3Req[2] && controlScript.alienRocks >= thisBuilding.level3Req[3])
                        {
                            instructions.alpha = 1.0f;
                            canConstruct = true;
                            canRepair = false;
                        }
                        else
                        {
                            instructions.alpha = 0.5f;
                            canConstruct = false;
                            canRepair = false;
                        }
                    }
                        constructionType.text = "Upgrade";
                }
                else
                {
                    instructions.alpha = 0.5f;
                    foreach (Text num in resourceNeeded)
                    {
                        num.text = "0";
                    }
                    constructionType.text = "Max Level";
                    canConstruct = false;
                    canRepair = false;
                }

            }
        }
    }

    public void ClickBuilding()
    {
        if (!Static.isExploding)
        {
            if (canConstruct)
            {
                construction.Play();
                if (thisBuilding.buildings.Count - 1 > thisBuilding.currentBuilding)
                {
                    switch (thisBuilding.currentBuilding)
                    {
                        case 0:
                            controlScript.metal -= thisBuilding.level1Req[0];
                            controlScript.copper -= thisBuilding.level1Req[1];
                            controlScript.shipParts -= thisBuilding.level1Req[2];
                            controlScript.alienRocks -= thisBuilding.level1Req[3];
                            thisBuilding.maxHP = thisBuilding.maxHP1;
                            thisBuilding.currentHP = thisBuilding.maxHP1;
                            if (thisBuilding.currentBuilding == 0)
                            {
                                showBuildScript.ShowBuildOptions();
                                if (!Static.startedGame)
                                {
                                    ding1.Play();
                                    foreach (GameObject child in tutorialItems)
                                    {
                                        child.SetActive(false);
                                    }
                                    Static.startedGame = true;
                                }
                                Static.isInside = true;
                                controlScript.gameObject.GetComponent<AudioSource>().Stop();
                                hummingSFX.Play();

                            }
                            if (thisBuilding.buildingName == "Training")
                            {
                                controller.m_WalkSpeed = 5;
                                controller.m_RunSpeed = 7;

                            }
                            break;
                        case 1:
                            controlScript.metal -= thisBuilding.level2Req[0];
                            controlScript.copper -= thisBuilding.level2Req[1];
                            controlScript.shipParts -= thisBuilding.level2Req[2];
                            controlScript.alienRocks -= thisBuilding.level2Req[3];
                            thisBuilding.maxHP = thisBuilding.maxHP2;
                            thisBuilding.currentHP = thisBuilding.maxHP2;
                            if (thisBuilding.buildingName == "Training")
                            {
                                controller.m_WalkSpeed = 8;
                                controller.m_RunSpeed = 9;

                            }
                            break;
                        case 2:
                            controlScript.metal -= thisBuilding.level3Req[0];
                            controlScript.copper -= thisBuilding.level3Req[1];
                            controlScript.shipParts -= thisBuilding.level3Req[2];
                            controlScript.alienRocks -= thisBuilding.level3Req[3];
                            thisBuilding.maxHP = thisBuilding.maxHP3;
                            thisBuilding.currentHP = thisBuilding.maxHP3;
                            if (thisBuilding.buildingName == "Training")
                            {
                                controller.m_WalkSpeed = 10;
                                controller.m_RunSpeed = 12;
                            }
                            break;
                    }
                    controlScript.UpdateInv();
                    thisBuilding.buildings[thisBuilding.currentBuilding].SetActive(false);
                    thisBuilding.currentBuilding++;
                    thisBuilding.buildings[thisBuilding.currentBuilding].SetActive(true);

                }
            }
            if (canRepair)
            {
                construction.Play();
                switch (thisBuilding.currentBuilding)
                {
                    case 1:
                        controlScript.metal -= thisBuilding.repairs1[0];
                        controlScript.copper -= thisBuilding.repairs1[1];
                        controlScript.shipParts -= thisBuilding.repairs1[2];
                        controlScript.alienRocks -= thisBuilding.repairs1[3];
                        thisBuilding.maxHP = thisBuilding.maxHP1;
                        thisBuilding.currentHP = thisBuilding.maxHP1;
                        break;
                    case 2:
                        controlScript.metal -= thisBuilding.repairs2[0];
                        controlScript.copper -= thisBuilding.repairs2[1];
                        controlScript.shipParts -= thisBuilding.repairs2[2];
                        controlScript.alienRocks -= thisBuilding.repairs2[3];
                        thisBuilding.maxHP = thisBuilding.maxHP2;
                        thisBuilding.currentHP = thisBuilding.maxHP2;
                        break;
                    case 3:
                        controlScript.metal -= thisBuilding.repairs2[0];
                        controlScript.copper -= thisBuilding.repairs2[1];
                        controlScript.shipParts -= thisBuilding.repairs2[2];
                        controlScript.alienRocks -= thisBuilding.repairs2[3];
                        thisBuilding.maxHP = thisBuilding.maxHP3;
                        thisBuilding.currentHP = thisBuilding.maxHP3;
                        break;
                }

                controlScript.UpdateInv();
            }
        }
    }









}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
   [SerializeField] CanvasGroup overlay, gameOverText;

    public void StartGameOver()
    {
        Static.interactionOn = false;
        StartCoroutine(gameOverFades());
        // pause interaction
        //Sequence endGame = DOTween.Sequence().OnComplete(() =>
        //{
        //    SceneManager.LoadScene("Title", LoadSceneMode.Single);
        //});

        //endGame.Insert(0, overlay.DOFade(1f, 1.5f));
        //endGame.Insert(1.5f, gameOverText.DOFade(1.0f, 1.5f));
        //endGame.Insert(3.5f, gameOverText.DOFade(0.0f, 1.0f));
        //endGame.Insert(6.0f, overlay.DOFade(1f, 1.5f));

    }

    IEnumerator gameOverFades()
    {
        overlay.DOFade(1f, 1.5f);
        yield return new WaitForSeconds(1.5f);
        gameOverText.DOFade(1.0f, 1.5f);
        yield return new WaitForSeconds(2.5f);
        gameOverText.DOFade(0.0f, 1.5f);
        yield return new WaitForSeconds(3.5f);
        SceneManager.LoadScene("Title", LoadSceneMode.Single);

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DayNightCycle : MonoBehaviour {
    [SerializeField] Text timer;
    [SerializeField] ControlsMain controlsScript;
    [SerializeField] ComCheck comScript;
    [SerializeField] BuildCrystal bedCrystal;
    //float dayTime = 100.0f;
    //float dayTime1 = 150.0f;
    //float dayTime2 = 200.0f;
    //float dayTime3 = 250.0f;
    //float nightTime = 24.0f;

    [SerializeField] AudioSource toneShortSFX;

    float dayTime = 100.0f;
    float dayTime1 = 140.0f;
    float dayTime2 = 190.0f;
    float dayTime3 = 250.0f;
    float nightTime = 60.0f;
    float timeLeft;

    [SerializeField] Renderer sand;
    [SerializeField] Material sandDay, sandNight, skyDay, skyNight;
    [SerializeField] CanvasGroup overlay, nightText, dayText;

    [SerializeField] List<GameObject> nightObjects = new List<GameObject>();


    void Start()
    {
        timeLeft = dayTime;
    }
    
    
    void Update()
    {
        if (Static.interactionOn)
        {
            if (timeLeft > 0.1f)
            {
                UpdateTime();
                timeLeft -= Time.deltaTime;
            }
            else
            {
                Static.interactionOn = false;
                timeLeft = 0;
                UpdateTime();
                StartCoroutine(ChangeTime());
            }
        }
    }
    
    void UpdateTime()
    {
        float minutes = Mathf.Floor(timeLeft / 60);
        float seconds = Mathf.Floor(timeLeft % 60);

        if (Static.isDay)
        {
            timer.text = "Day: " + minutes.ToString() + ":" + seconds.ToString("00");
        }
        else
        {
            timer.text = "Night: " + minutes.ToString() + ":" + seconds.ToString("00");
        }
    }

    IEnumerator ChangeTime()
    {
        comScript.CheckForCom();
        yield return new WaitForSeconds(2.5f);
        if (Static.isDay)
        {
            // MAKE NIGHT TIME
            toneShortSFX.Play();

            foreach (GameObject item in nightObjects)
            {
                item.SetActive(true);
            }

            Static.isDay = false;
            sand.material = sandNight;
            RenderSettings.skybox = skyNight;
            yield return new WaitForSeconds(1.5f);
            timeLeft = nightTime;
        }
        else
        {
            // MAKE DAY TIME
            foreach (GameObject item in nightObjects)
            {
                item.SetActive(false);
            }
            Static.isDay = true;
            controlsScript.ScatterResources();
            sand.material = sandDay;
            RenderSettings.skybox = skyDay;
            yield return new WaitForSeconds(1.5f);
            timeLeft = dayTime;
            switch (bedCrystal.thisBuilding.currentBuilding)
            {
                case 1:
                    timeLeft = dayTime1;
                    break;
                case 2:
                    timeLeft = dayTime2;
                    break;
                case 3:
                    timeLeft = dayTime3;
                    break;
            }
        }
        Static.interactionOn = true;
    }

}

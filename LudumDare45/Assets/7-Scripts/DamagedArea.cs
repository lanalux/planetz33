﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagedArea : MonoBehaviour
{
    void Start()
    {
        Attacks.startExplosion += GotHit;
    }

    void GotHit()
    {
        StartCoroutine(BriefExplosion());


    }

    void OnDisable()
    {
        Attacks.startExplosion -= GotHit;
    }


    IEnumerator BriefExplosion()
    {
        float rand = Random.Range(0, 0.5f);
        yield return new WaitForSeconds(rand);
        foreach (Transform child in this.gameObject.transform)
        {
            child.GetComponent<ParticleSystem>().Play();
        }
        float rand2 = Random.Range(1, 2.5f);
        yield return new WaitForSeconds(rand2);
        foreach (Transform child in this.gameObject.transform)
        {
            child.GetComponent<ParticleSystem>().Stop();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Playercast : MonoBehaviour
{
    [SerializeField] ControlsMain controls;
    Camera playerCam;
    float interactionDistance = 2.0f;
    float timeToPickUp = 0.3f;
    [SerializeField] LayerMask interactionLayer;
    [SerializeField] CanvasGroup cursorOutline;
    [SerializeField] Transform holdingPosObject;
    [SerializeField] GameObject crystalUI, pickUpUI;
    GameObject currentObject;
    //GameObject waitingToBuild;
    AudioSource pickUpSFX;


    void Start()
    {
        playerCam = this.GetComponent<Camera>();
        pickUpSFX = this.GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Static.interactionOn)
        {
            RaycastHit hit;
            Ray ray = playerCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            if (Physics.Raycast(ray, out hit, interactionDistance, interactionLayer))
            {
                cursorOutline.alpha = 1;
                currentObject = hit.transform.gameObject;
                BuildCrystal crystalScript = currentObject.GetComponent<BuildCrystal>();
                if (crystalScript != null)
                {
                    crystalScript.HoverBuilding(currentObject);
                    ShowUI(crystalUI);
                }
                if (currentObject.GetComponent<PickUp>() != null)
                    ShowUI(pickUpUI);
            }
            else
            {
                cursorOutline.alpha = 0;
                currentObject = null;
                HideUI(crystalUI);
                HideUI(pickUpUI);
            }



            if (Input.GetMouseButtonDown(0))
            {
                if (currentObject != null)
                {
                    PickUp pickUpScript = currentObject.GetComponent<PickUp>();
                    if (pickUpScript != null)
                    {
                        pickUpSFX.Play();
                        switch (pickUpScript.resource)
                        {
                            case 0:
                                controls.metal++;
                                break;
                            case 1:
                                controls.copper++;
                                break;
                            case 2:
                                controls.shipParts++;
                                break;
                            case 3:
                                controls.alienRocks++;
                                break;
                        }
                        controls.UpdateInv();

                        Vector3 startPos = currentObject.GetComponent<PickUp>().startPos;
                        currentObject.transform.position = startPos;
                        //Destroy(currentObject);




                        //Vector3 holdingPos = holdingPosObject.position;
                        //currentObject.GetComponent<Rigidbody>().isKinematic = true;
                        //currentObject.GetComponent<Collider>().enabled = false;
                        //currentObject.transform.DOMove(holdingPos, timeToPickUp).OnComplete(() =>
                        //{
                        //    Destroy(currentObject);
                        //});

                    }
                    BuildCrystal crystalScript = currentObject.GetComponent<BuildCrystal>();
                    if (crystalScript != null)
                    {
                        crystalScript.ClickBuilding();
                    }
                }
            }

        }
    }


    




    
    void ShowUI(GameObject uiToShow)
    {
        if(!uiToShow.activeSelf)
            uiToShow.SetActive(true);
    }
    void HideUI(GameObject uiToShow)
    {
        if(uiToShow.activeSelf)
            uiToShow.SetActive(false);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Building
{
    public string buildingName;
    public List<GameObject> buildings = new List<GameObject>();
    public int currentBuilding = 0;
    public float maxHP = 100;
    public float currentHP = 100;
    public float maxHP1 = 100;
    public float maxHP2 = 200;
    public float maxHP3 = 300;

    public List<int> level1Req = new List<int>();
    public List<int> level2Req = new List<int>();
    public List<int> level3Req = new List<int>();

    public List<int> repairs1 = new List<int>();
    public List<int> repairs2 = new List<int>();
    public List<int> repairs3 = new List<int>();


    public Building(string buildingName, List<GameObject> buildings, int currentBuilding, float maxHP, float currentHP, List<int> level1Req, List<int> level2Req, List<int> level3Req, float maxHP1, float maxHP2, float maxHP3, List<int> repairs1, List<int> repairs2, List<int> repairs3)
    {
        this.buildingName = buildingName;
        this.buildings = buildings;
        this.currentBuilding = currentBuilding;
        this.maxHP = maxHP;
        this.currentHP = currentHP;
        this.level1Req = level1Req;
        this.level2Req = level2Req;
        this.level3Req = level3Req;
        this.maxHP1 = maxHP1;
        this.maxHP2 = maxHP2;
        this.maxHP3 = maxHP3;
        this.repairs1 = repairs1;
        this.repairs2 = repairs2;
        this.repairs3 = repairs3;

    }
}

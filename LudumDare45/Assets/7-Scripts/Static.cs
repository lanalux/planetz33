﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Static : MonoBehaviour
{
    public static bool interactionOn = false;
    public static bool isInside = false;
    public static bool isDay = true;
    public static int radioLevel = 0;
    public static bool startedGame = false;
    public static bool isExploding = false;

}

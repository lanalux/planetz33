﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCollider : MonoBehaviour
{
    public bool isInside;
    [SerializeField] AudioSource outsideSFX, insideSFX, passSFX;

    void OnTriggerEnter(Collider col)
    {
        if (isInside)
        {
            Static.isInside = true;
            outsideSFX.Stop();
            insideSFX.Play();
        }
        else
        {
            Static.isInside = false;
            outsideSFX.Play();
            insideSFX.Stop();
        }
    }

    void OnTriggerExit(Collider col)
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ControlsMain : MonoBehaviour
{
    [SerializeField] CanvasGroup overlay;
    [SerializeField] Text metalCount, copperCount, shipPartsCount, alienRocksCount;
    [SerializeField] GameObject metalParent, copperParent, shipPartsParent, alienRocksParent;
    public int metal = 0;
    public int copper = 0;
    public int shipParts = 0;
    public int alienRocks = 0;


    float metalRadius = 6.0f;
    float copperRadius = 6.0f;
    float shipPartsRadius = 20.0f;
    float alienRocksRadius = 90.0f;

    float maxX = 250f;
    float minX = -250f;
    float maxZ = 250f;
    float minZ = -250;
    //float maxX = 150f;
    //float minX = -150f;
    //float maxZ = 150f;
    //float minZ = -150;
    //float maxX = 50f;
    //float minX = -50f;
    //float maxZ = 50f;
    //float minZ = -50;
    float resourceHeight = 0.9f;

    bool quitScreenIsUp = false;
    [SerializeField] GameObject quitPopup;

    void Start()
    {
        ScatterResources();
        Sequence fadeIn = DOTween.Sequence().OnComplete(() =>
        {
            Static.interactionOn = true;
        });
        fadeIn.Insert(0.5f, overlay.DOFade(0, 0.5f));
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)){
            quitPopup.SetActive(true);
            quitScreenIsUp = true;
        }
        if (Input.GetKeyDown(KeyCode.Y)){
            if (quitScreenIsUp)
            {
                Application.Quit();
            }
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            if (quitScreenIsUp)
            {
                quitPopup.SetActive(false);
                quitScreenIsUp = false;
                Cursor.visible = false;
            }
        }

    }

    public void UpdateInv()
    {
        metalCount.text = metal.ToString();
        copperCount.text = copper.ToString();
        shipPartsCount.text = shipParts.ToString();
        alienRocksCount.text = alienRocks.ToString();
    }

    //public void RescatterResources()
    //{
    //    foreach (Transform child in resourceParent.transform)
    //    {
    //        child.position = child.GetComponent<PickUp>().startPos;
    //    }
    //    ScatterResources();
    //}

    public void ScatterResources()
    {
        foreach (Transform child in metalParent.transform)
        {
            float randX = Random.Range(minX, maxX);
            float randZ = Random.Range(minZ, maxZ);

            while (randX > -metalRadius && randX < metalRadius && randZ > -metalRadius && randZ < metalRadius)
            {
                randX = Random.Range(minX, maxX);
                randZ = Random.Range(minZ, maxZ);
            }
            child.position = new Vector3(randX, resourceHeight, randZ);
        }
        foreach (Transform child in alienRocksParent.transform)
        {
            float randX = Random.Range(minX, maxX);
            float randZ = Random.Range(minZ, maxZ);

            while (randX > -alienRocksRadius && randX < alienRocksRadius && randZ > -alienRocksRadius && randZ < alienRocksRadius)
            {
                randX = Random.Range(minX, maxX);
                randZ = Random.Range(minZ, maxZ);
            }
            child.position = new Vector3(randX, resourceHeight, randZ);
        }
        foreach (Transform child in copperParent.transform)
        {
            float randX = Random.Range(minX, maxX);
            float randZ = Random.Range(minZ, maxZ);

            while (randX > -copperRadius && randX < copperRadius && randZ > -copperRadius && randZ < copperRadius)
            {
                randX = Random.Range(minX, maxX);
                randZ = Random.Range(minZ, maxZ);
            }
            child.position = new Vector3(randX, resourceHeight, randZ);
        }
        foreach (Transform child in shipPartsParent.transform)
        {
            float randX = Random.Range(minX, maxX);
            float randZ = Random.Range(minZ, maxZ);

            while (randX > -shipPartsRadius && randX < shipPartsRadius && randZ > -shipPartsRadius && randZ < shipPartsRadius)
            {
                randX = Random.Range(minX, maxX);
                randZ = Random.Range(minZ, maxZ);
            }
            child.position = new Vector3(randX, resourceHeight, randZ);
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class ComCheck : MonoBehaviour {
    [SerializeField] CanvasGroup overlay, winText;
    [SerializeField] GameObject comBuilding;
    [SerializeField] AudioSource radioSFX;

    public void CheckForCom()
    {
        if (comBuilding.activeSelf)
        {
            BuildCrystal crystalScript = comBuilding.transform.GetChild(0).GetComponent<BuildCrystal>();
            int chanceOfRescue = 0;
            if (crystalScript.thisBuilding.currentBuilding == 1)
            {
                chanceOfRescue = Random.Range(0, 20);
            }
            else if (crystalScript.thisBuilding.currentBuilding == 2)
            {
                chanceOfRescue = Random.Range(0, 17);
            }
            else if (crystalScript.thisBuilding.currentBuilding == 3)
            {
                chanceOfRescue = Random.Range(0, 5);
            }
            Debug.Log(chanceOfRescue);
            if (chanceOfRescue == 2)
            {
                radioSFX.Play();
                WinGame();
            }
        }
    }

    public void WinGame()
    {
        Static.interactionOn = false;

        Sequence winGame = DOTween.Sequence().OnComplete(() =>
        {
            SceneManager.LoadScene("Title");
        });
        winGame.Insert(0, overlay.DOFade(1.0f, 1.0f));
        winGame.Insert(1.5f, winText.DOFade(1.0f, 1.0f));
        winGame.Insert(4.5f, winText.DOFade(0.0f, 1.0f));
    }
}

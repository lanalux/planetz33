﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideBuildOptions : MonoBehaviour
{

    public List<GameObject> extraBuildOptions = new List<GameObject>();

    public void ShowBuildOptions()
    {
        foreach (GameObject option in extraBuildOptions)
        {
            option.SetActive(true);
        }
    }

    public void HideBuildOptions()
    {
        foreach (GameObject option in extraBuildOptions)
        {
            option.SetActive(false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class ControlsTitle : MonoBehaviour
{

    [SerializeField] CanvasGroup startScreen, introScreen, overlay;
    [SerializeField] AudioSource blip;

    public void StartNewGame()
    {
        blip.Play();
        startScreen.DOFade(0f, 2.0f).OnComplete(() =>
        {
            startScreen.gameObject.SetActive(false);
            introScreen.gameObject.SetActive(true);
            introScreen.DOFade(1.0f, 2.0f);

        });
    }
    public void BackToStartScreen()
    {
        blip.Play();
        introScreen.DOFade(0f, 0.8f).OnComplete(() =>
        {
            introScreen.gameObject.SetActive(false);
            startScreen.gameObject.SetActive(true);
            startScreen.DOFade(1.0f, 0.8f);
        });
    }

    public void GoToMainScene()
    {

        blip.Play();
        introScreen.DOFade(0f, 2.0f).OnComplete(() =>
        {
            overlay.DOFade(1f, 2.0f).OnComplete(() =>
            {
                SceneManager.LoadScene("Main");
            });
        });
    }

    public void QuitGame()
    {
        blip.Play();
        Application.Quit();
    }
}
